# Semantic Version Control (semverctl)

This is a simple tool to manage your project's versioning using semantic versioning. It is a command line tool that can be used to increment the version of your project. It can also sort version numbers from command line arguments or standard input.

It uses [semver 2.0.0](https://semver.org/spec/v2.0.0.html) specification for version numbers.

## Usage

```sh
# bump version number (print result to standard output)
semverctl bump <version> <major|minor|patch|prerelease>

# sort version numbers from arguments
semverctl sort <version>...

# sort version numbers from standard input (one per line)
semverctl sort

# compare two version numbers (prints -1, 0, or 1 to standard output)
semverctl compare <version1> <version2>

# validate version number (returns 0 if valid, 1 if invalid)
semverctl validate <version>
```

## Examples

```sh
# bump patch version number => 1.2.4
semverctl bump 1.2.3 patch

# bump prerelease version number (ending in "." means autoincrement)=> 1.2.3-alpha.1
semverctl bump 1.2.3 prerelease -p alpha.

# bump prerelease version number => 1.2.3-alpha.2
semverctl bump 1.2.3-alpha.1 prerelease -p alpha.

# bump prerelease version number => 1.2.3-alpha.17
semverctl bump 1.2.3 prerelease -p alpha.17

# sort version numbers from arguments => 1.2.1 1.2.2 1.2.3
semverctl sort 1.2.3 1.2.1 1.2.2

# sort version numbers from standard input => 1.2.1 1.2.2 1.2.3
echo -e "1.2.3\n1.2.1\n1.2.2" | semverctl sort

# compare two version numbers => 1
semverctl compare 1.2.3 1.2.1

# validate version number => 0
semverctl validate 1.2.3

# validate version number => 1
semverctl validate 1.4-alpha
```
