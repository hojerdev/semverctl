package main

import "os"
import "gitlab.com/hojerdev/semverctl/cmd"

func main() {
	err := cmd.New().Execute()
	if err != nil {
		os.Exit(1)
	}
}
