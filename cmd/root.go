package cmd

import (
	"github.com/spf13/cobra"
)

// current version of the package (will be dynamically set to a semantic version by the build process)
var Version = "0.0.0-dev"

// Create a new root command instance
func New() *cobra.Command {
	cmd := &cobra.Command{
		Use:          "semverctl",
		Short:        "Tool to work with version numbers according to Semantic Versioning (https://semver.org/spec/v2.0.0.html)",
		Version:      Version,
		SilenceUsage: true,
	}

	cmd.AddCommand(
		newSortCmd(),
		newBumpCmd(),
		newCompareCmd(),
		newGetCmd(),
		newValidateCmd(),
	)

	return cmd
}
