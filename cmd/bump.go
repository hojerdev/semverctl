package cmd

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/coreos/go-semver/semver"
	"github.com/spf13/cobra"
)

type bumpCmd struct {
	cobra.Command
	Metadata   string
	PreRelease string
}

func newBumpCmd() *cobra.Command {
	cmd := bumpCmd{
		Command: cobra.Command{
			Use:   "bump OPERATION VERSION",
			Short: "bump version numbers",
			Args:  cobra.ExactArgs(2),
		},
	}

	cmd.RunE = func(_ *cobra.Command, args []string) error {
		return cmd.Execute(args)
	}

	cmd.Flags().StringVarP(&cmd.Metadata, "metadata", "m", "", "build metadata")
	cmd.Flags().StringVarP(&cmd.PreRelease, "prerelease", "p", "", "prerelease")

	return &cmd.Command
}

func (cmd *bumpCmd) Execute(args []string) error {
	version, err := semver.NewVersion(args[1])
	if err != nil {
		return err
	}

	switch args[0] {
	case "major":
		version.BumpMajor()
	case "minor":
		version.BumpMinor()
	case "patch":
		version.BumpPatch()
	case "prerelease":
	default:
		return fmt.Errorf("unknown operation %s", args[0])
	}

	if strings.HasSuffix(cmd.PreRelease, ".") {
		element := 0

		// check whether version begins with the given prerelease string
		if strings.HasPrefix(string(version.PreRelease), cmd.PreRelease) {
			// extract the current number from the prerelease string
			suffix := string(version.PreRelease[len(cmd.PreRelease):])
			element, err = strconv.Atoi(strings.Split(suffix, ".")[0])
			if err != nil {
				element = 0
			}
		}

		version.PreRelease = semver.PreRelease(cmd.PreRelease + strconv.Itoa(element+1))
	} else {
		version.PreRelease = semver.PreRelease(cmd.PreRelease)
	}
	version.Metadata = cmd.Metadata

	fmt.Fprintln(cmd.OutOrStdout(), version)

	return nil
}
