package cmd

import (
	"fmt"

	"github.com/coreos/go-semver/semver"
	"github.com/spf13/cobra"
)

type compareCmd struct {
	cobra.Command
}

func newCompareCmd() *cobra.Command {
	cmd := compareCmd{
		Command: cobra.Command{
			Use:   "compare VERSION1 VERSION2",
			Short: "compare two version numbers (prints -1, 0 or 1)",
			Long: `compare two version numbers

  if VERSION1 < VERSION2, prints -1
  if VERSION1 == VERSION2, prints 0
  if VERSION1 > VERSION2, prints 1`,
			Args: cobra.ExactArgs(2),
		},
	}

	cmd.RunE = func(_ *cobra.Command, args []string) error {
		return cmd.Execute(args)
	}

	return &cmd.Command
}

func (cmd *compareCmd) Execute(args []string) error {
	version1, err := semver.NewVersion(args[0])
	if err != nil {
		return err
	}

	version2, err := semver.NewVersion(args[1])
	if err != nil {
		return err
	}

	fmt.Fprintln(cmd.OutOrStdout(), version1.Compare(*version2))

	return nil
}
