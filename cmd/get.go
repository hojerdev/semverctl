package cmd

import (
	"fmt"

	"github.com/coreos/go-semver/semver"
	"github.com/spf13/cobra"
)

type getCmd struct {
	cobra.Command
}

func newGetCmd() *cobra.Command {
	cmd := getCmd{
		Command: cobra.Command{
			Use:   "get ELEMENT VERSION",
			Short: "extract an element from a version number (major, minor, patch, prerelease, metadata, release)",
			Long: `extract an element from a version number

	major       print the major version number
	minor       print the minor version number
	patch       print the patch version number
	prerelease  print the prerelease version number
	metadata    print the build metadata
	release     print the release version number (without prerelease and metadata)`,
			Args: cobra.ExactArgs(2),
		},
	}

	cmd.RunE = func(_ *cobra.Command, args []string) error {
		return cmd.Execute(args)
	}

	return &cmd.Command
}

func (cmd *getCmd) Execute(args []string) error {
	version := semver.New(args[1])

	switch args[0] {
	case "major":
		fmt.Fprintln(cmd.OutOrStdout(), version.Major)
	case "minor":
		fmt.Fprintln(cmd.OutOrStdout(), version.Minor)
	case "patch":
		fmt.Fprintln(cmd.OutOrStdout(), version.Patch)
	case "prerelease":
		fmt.Fprintln(cmd.OutOrStdout(), version.PreRelease)
	case "metadata":
		fmt.Fprintln(cmd.OutOrStdout(), version.Metadata)
	case "release":
		version.PreRelease = ""
		version.Metadata = ""
		fmt.Fprintln(cmd.OutOrStdout(), version)
	default:
		return fmt.Errorf("unknown element '%s'", args[0])
	}

	return nil
}
