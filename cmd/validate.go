package cmd

import (
	"github.com/coreos/go-semver/semver"
	"github.com/spf13/cobra"
)

type validateCmd struct {
	cobra.Command
}

func newValidateCmd() *cobra.Command {
	cmd := validateCmd{
		Command: cobra.Command{
			Use:   "validate VERSION",
			Short: "validate a version number (returns 0 if valid, 1 otherwise)",
			Args:  cobra.ExactArgs(1),
		},
	}

	cmd.RunE = func(_ *cobra.Command, args []string) error {
		return cmd.Execute(args)
	}

	return &cmd.Command
}

func (cmd *validateCmd) Execute(args []string) error {
	_, err := semver.NewVersion(args[0])
	if err != nil {
		return err
	}

	return nil
}
