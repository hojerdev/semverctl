package cmd

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCompare(t *testing.T) {
	var tests = []struct {
		version1 string
		version2 string
		result   string
		error    bool
	}{
		{"1.2.3", "1.2.3", "0\n", false},
		{"1.2.3", "1.2.4", "-1\n", false},
		{"1.2.4", "1.2.3", "1\n", false},
		{"1.2.3", "1.2.3-alpha", "1\n", false},
		{"1.2.3-alpha", "1.2.3", "-1\n", false},
		{"1.2.3-alpha", "1.2.3-alpha", "0\n", false},
		{"1.2.3-alpha", "1.2.3-alpha.1", "-1\n", false},
		{"1.2.3-alpha.1", "1.2.3-alpha", "1\n", false},
		{"1.2.3-alpha.1", "1.2.3-alpha.1", "0\n", false},
		{"1.2.3-alpha.1", "1.2.3-alpha.1.2", "-1\n", false},
		{"1.2.3-alpha.1.2", "1.2.3-alpha.1", "1\n", false},
		{"1.2.3-alpha.1.2", "1.2.3-alpha.1.2", "0\n", false},
		{"1.2.3-alpha.1.2", "1.2.3-alpha.1.2+meta", "0\n", false},
		{"1.2.3-alpha.1.2+meta", "1.2.3-alpha.1.2", "0\n", false},
		{"1.2.3-alpha.1.2+meta", "1.2.3-alpha.1.2+meta", "0\n", false},
		{"1.2.3-alpha.1.2+meta.1", "1.2.3-alpha.1.2+meta.2", "0\n", false},
		{"1.1", "1.2", "Error: 1.1 is not in dotted-tri format\n", true},
	}

	var cmd = newCompareCmd()
	cmd.SilenceUsage = true

	// Simulate command execution
	for _, test := range tests {
		buf := new(bytes.Buffer)
		cmd.SetOutput(buf)
		cmd.SetArgs([]string{test.version1, test.version2})
		err := cmd.Execute()

		assert.Equal(t, test.result, buf.String())
		if test.error {
			assert.Error(t, err)
		} else {
			assert.NoError(t, err)
		}
	}
}
