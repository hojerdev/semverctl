package cmd

import (
	"bufio"
	"fmt"
	"os"

	"github.com/coreos/go-semver/semver"
	"github.com/spf13/cobra"
)

func newSortCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "sort [VERSION...]",
		Short: "sort version numbers",
		Args:  cobra.ArbitraryArgs,
		RunE:  sortCmd,
	}
}

func sortCmd(cmd *cobra.Command, args []string) error {
	if len(args) == 0 {
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			args = append(args, scanner.Text())
		}
	}

	versions := semver.Versions{}
	for _, arg := range args {
		version, err := semver.NewVersion(arg)
		if err != nil {
			return err
		}
		versions = append(versions, version)
	}

	semver.Sort(versions)

	for _, version := range versions {
		fmt.Fprintln(cmd.OutOrStdout(), version)
	}

	return nil
}
