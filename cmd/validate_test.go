package cmd

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidate(t *testing.T) {
	var tests = []struct {
		version string
		valid   bool
	}{
		{"0.0.0", true},
		{"1.2.3", true},
		{"1.2.3-alpha", true},
		{"1.2.3-alpha.1", true},
		{"1.2.3-alpha.1.2", true},
		{"1.2.3-alpha.1.2+meta", true},
		{"99999999999999999.99999999999999999.99999999999999999+test", true},
		{"1.2.3-ALPHA", true},
		{"1.2.3-A.b.c.d.-", true},
		{"1.2.3-Ab.cd.ef.gh-test.0+test", true},
		{"1.2.3--", true},
		{"1.1", false},
		{"1.1.b", false},
		{"1", false},
		{"1.2.3-.", false},
		{"1.2.3-ab(c", false},
	}

	var cmd = newValidateCmd()

	// Simulate command execution
	for _, test := range tests {
		buf := new(bytes.Buffer)
		cmd.SetOutput(buf)
		cmd.SetArgs([]string{test.version})
		err := cmd.Execute()

		if test.valid {
			assert.NoError(t, err)
		} else {
			assert.Error(t, err)
		}
	}
}
