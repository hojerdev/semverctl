package cmd

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSort(t *testing.T) {
	var tests = []struct {
		args   []string
		output string
		valid  bool
	}{
		{[]string{"1.2.3"}, "1.2.3\n", true},
		{[]string{"1.2.3-alpha.2", "1.2.3-alpha.2+meta.2", "1.2.4", "1.2.3-alpha.2+meta.1", "1.2.1"}, "1.2.1\n1.2.3-alpha.2\n1.2.3-alpha.2+meta.2\n1.2.3-alpha.2+meta.1\n1.2.4\n", true},
		{[]string{"1.2.3", "1.2.3-alpha.2", "1.2.3-alpha.2+meta.2", "1.2.4", "1.2.3-alpha.2+meta.1", "1.2.1"}, "1.2.1\n1.2.3-alpha.2\n1.2.3-alpha.2+meta.2\n1.2.3-alpha.2+meta.1\n1.2.3\n1.2.4\n", true},
		{[]string{"1.2.3.4"}, "Error: strconv.ParseInt: parsing \"3.4\": invalid syntax\n", false},
	}

	var cmd = newSortCmd()
	cmd.SilenceUsage = true

	// Simulate command execution
	for _, test := range tests {
		buf := new(bytes.Buffer)
		cmd.SetOutput(buf)
		cmd.SetArgs(test.args)
		err := cmd.Execute()

		assert.Equal(t, test.output, buf.String())
		if test.valid {
			assert.NoError(t, err)
		} else {
			assert.Error(t, err)
		}
	}
}
