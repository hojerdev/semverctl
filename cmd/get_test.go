package cmd

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGet(t *testing.T) {
	var tests = []struct {
		element string
		version string
		result  string
		error   bool
	}{
		{"major", "1.2.3", "1\n", false},
		{"minor", "1.2.3", "2\n", false},
		{"patch", "1.2.3", "3\n", false},
		{"prerelease", "1.2.3-alpha", "alpha\n", false},
		{"prerelease", "1.2.3-alpha.1", "alpha.1\n", false},
		{"prerelease", "1.2.3-alpha.1.2", "alpha.1.2\n", false},
		{"prerelease", "1.2.3-alpha.1.2+meta", "alpha.1.2\n", false},
		{"metadata", "1.2.3-alpha.1.2+meta", "meta\n", false},
		{"release", "1.2.3-alpha.1.2+meta", "1.2.3\n", false},
		{"unknown", "1.2.3-alpha.1.2+meta", "Error: unknown element 'unknown'\n", true},
	}

	var cmd = newGetCmd()
	cmd.SilenceUsage = true

	// Simulate command execution
	for _, test := range tests {
		buf := new(bytes.Buffer)
		cmd.SetOutput(buf)
		cmd.SetArgs([]string{test.element, test.version})
		err := cmd.Execute()

		assert.Equal(t, test.result, buf.String())
		if test.error {
			assert.Error(t, err)
		} else {
			assert.NoError(t, err)
		}
	}
}
