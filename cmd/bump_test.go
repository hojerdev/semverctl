package cmd

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBump(t *testing.T) {
	var tests = []struct {
		element    string
		version    string
		prerelease string
		metadata   string
		result     string
		error      bool
	}{
		{"major", "1.2.3", "", "", "2.0.0\n", false},
		{"minor", "1.2.3", "", "", "1.3.0\n", false},
		{"patch", "1.2.3", "", "", "1.2.4\n", false},
		{"prerelease", "1.2.3", "", "", "1.2.3\n", false},
		{"major", "1.2.3", "alpha.1", "", "2.0.0-alpha.1\n", false},
		{"minor", "1.2.3", "alpha.1", "", "1.3.0-alpha.1\n", false},
		{"patch", "1.2.3", "alpha.1", "", "1.2.4-alpha.1\n", false},
		{"prerelease", "1.2.3", "alpha.1", "", "1.2.3-alpha.1\n", false},
		{"major", "1.2.3", "alpha.1", "meta", "2.0.0-alpha.1+meta\n", false},
		{"minor", "1.2.3", "alpha.1", "meta", "1.3.0-alpha.1+meta\n", false},
		{"patch", "1.2.3", "alpha.1", "meta", "1.2.4-alpha.1+meta\n", false},
		{"prerelease", "1.2.3", "alpha.1", "meta", "1.2.3-alpha.1+meta\n", false},
		{"major", "1.2.3", "alpha.", "", "2.0.0-alpha.1\n", false},
		{"minor", "1.2.3", "alpha.", "", "1.3.0-alpha.1\n", false},
		{"patch", "1.2.3", "alpha.", "", "1.2.4-alpha.1\n", false},
		{"prerelease", "1.2.3", "alpha.", "", "1.2.3-alpha.1\n", false},
		{"major", "1.2.3-alpha.1", "alpha.", "", "2.0.0-alpha.1\n", false},
		{"minor", "1.2.3-alpha.1", "alpha.", "", "1.3.0-alpha.1\n", false},
		{"patch", "1.2.3-alpha.1", "alpha.", "", "1.2.4-alpha.1\n", false},
		{"prerelease", "1.2.3-alpha.1", "alpha.", "", "1.2.3-alpha.2\n", false},
		{"prerelease", "1.2.3-alpha.2", "alpha.", "", "1.2.3-alpha.3\n", false},
		{"prerelease", "1.2.3-alpha.2", "beta.", "", "1.2.3-beta.1\n", false},
		{"prerelease", "1.2.3", "alpha", "", "1.2.3-alpha\n", false},
		{"prerelease", "1.2.3-alpha", "beta", "", "1.2.3-beta\n", false},
		{"prerelease", "1.2.3-alpha.1", "beta", "", "1.2.3-beta\n", false},
		{"prerelease", "1.2.3-alpha.1.2", "beta", "", "1.2.3-beta\n", false},
		{"prerelease", "1.2.3-alpha.1.2+meta", "beta", "", "1.2.3-beta\n", false},
		{"prerelease", "1.2.3-alpha.1.2+meta", "", "test", "1.2.3+test\n", false},
		{"prerelease", "1.2.3-alpha.10", "alpha.", "", "1.2.3-alpha.11\n", false},
		{"prerelease", "1.2.3-alpha.10.1", "alpha.", "", "1.2.3-alpha.11\n", false},
		{"prerelease", "1.2.3-alpha.10.9.8.7.6", "alpha.", "", "1.2.3-alpha.11\n", false},
	}

	var cmd = newBumpCmd()
	cmd.SilenceUsage = true

	// Simulate command execution
	for _, test := range tests {
		buf := new(bytes.Buffer)
		cmd.SetOutput(buf)
		cmd.SetArgs([]string{test.element, test.version})
		assert.NoError(t, cmd.Flags().Set("prerelease", test.prerelease))
		assert.NoError(t, cmd.Flags().Set("metadata", test.metadata))
		err := cmd.Execute()

		assert.Equal(t, test.result, buf.String())
		if test.error {
			assert.Error(t, err)
		} else {
			assert.NoError(t, err)
		}
	}
}
